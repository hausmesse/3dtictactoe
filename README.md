# Allgemein
Dies ist ein Projekt, in dem ein Arduino einen 4x4x4 großen LED-Würfel ansteuern kann.\
Ziel ist die Umsetzung eines 3 dimensionalen Tic Tac Toe Spiels, auch bekannt als Qubic.

## Pinbelegung
Die Pinbelegung der Buttons und der Adafruit LED-Bänder ist in der Hauptdatei festgelegt.\
[Pinbelegung ansehen](https://gitlab.com/hausmesse/3dtictactoe/-/blob/master/arduinoProject/arduinoProject.ino)

## Globale Variablen
Das **cube[4][4][4]** Array ist die 3 dimensionale Repräsentation des LED-Würfels.\
Jedes Feld des Arrays kann mit einem Wert gefüllt werden. \
z.B. mit einer der folgenden Konstanten:
### Kostanten:
- EMPTY_FIELD
- RED
- GREEN
- BLUE
- SELECTED_FIELD

## Funktionen

### show(*int cube[FIELD_SIZE][FIELD_SIZE][FIELD_SIZE]*)
Die Funktion show() nimmt den **cube** entgegen und zeigt ihn auf dem LED-Würfel an.\
In der Funktion ist für jede **Konstante** eine Farbe definiert, die dann auf der zugehörigen LED ausgegeben wird.\
(z.B. **EMPTY_FIELD** = Schwarz)

### copyCube(int fromCube[FS][FS][FS], int toCube[FS][FS][FS])
*FS = FIELD_SIZE*\
Die Funktion **copyCube()** kopiert alle Werte aus dem **fromCube** Array in das **toCube** Array. Damit kann man eine Kopie des aktuellen Spielfeldes/Würfels erzeugen und manipulieren, ohne dass das original Array verändert werden muss.

### isButtonPressed()
Die folgenden 4 Funktionen können für die einfache Abfrage der Buttons verwendet werden.
- isButton1Pressed()
- isButton2Pressed()
- isButton3Pressed()
- isButton4Pressed()

### showText()/ showNumber()
Die beiden Funktionen **showText()** und **showNumber()** können für das Debugging verwendet werden.

### showErrorSreen()
Setzt alle LEDs auf ROT, ohne **cube** zu verändern.

### resetCube()
Setzt alle Felder in **cube** auf 0.

## Main

Die Hauptfunktion ist die **loop()** Funktion des Arduinos. Diese ruft die Hauptfunktion des GameChoosers auf.

# Spiele Auswahl Menü (GameChooser)
Die **gameChooserLoop()** zeigt das Icon des aktuell ausgewählten Spiels an. Mit Button2 kann zwischen den Spielen gewechselt werden.\
Durch drücken von Button1 wid die Auswahl bestätigt. Ab dem Zeitpunkt der Bestätigung hat die **gameChooserLoop()** nur noch die Aufgabe, immer wieder die mainLoop() Funktion des ausgewählten Spiels aufzurufen.\
(z.B. die Funktion **tictactoeLoop()** aus der Datei  **tictactoe.ino**)

## Spiel hinzufügen
Neue Spiele werden in einer neuen Datei angelegt, und benötigen diese zwei Funktionen:
- dateiname**Icon**(int iconArray[FS][FS][FS])
- dateiname**Loop**()

Zusätzlich müssen die beiden Funktionen in der **gameChooser.ino** in die Funktion **showGameIconOrCallMainLoop()** eingetragen werden.

## Spiele Icons
### *dateiname*Icon(int iconArray[FS][FS][FS]) 
*FS = FIELD_SIZE*\
Die Spiele Icons sind oberhalb der SpielDatei (z.B. in der snake.ino) als Funktion definiert.\
Diese Funktionen nehmen als Parameter ein mehrdimensionales Array, in das sie das Icon schreiben.\
Z.B. die Funktion **snakeIcon(iconArray)** schreibt das Snake-Icon in das übergebene **iconArray**.\
Dieses Array wird dann mit **show(iconArray)** im GameChooser ausgegeben.

# 3D Tic Tac Toe
Das Hauptspiel ist in der Datei **tictactoe.ino**. Hier sind auch Funktionen definiert, die nur für dieses Spiel benötigt werden

### waitForUserToSelectField(int cube[FS][FS][FS], int value)
*FS = FIELD_SIZE*\
Diese Funktion bekommt den aktuellen Stand des Würfels als Parameter. Dann kann der User mit den Buttons frei ein Feld auswählen. Wenn sich der User für ein Feld entschieden hat, wird in das gewählte Feld der Wert von **value** geschrieben und die Funktion wird beendet.