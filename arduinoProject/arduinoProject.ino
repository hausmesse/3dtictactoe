#include <Adafruit_NeoPixel.h>
#define NUMPIXELS 7*4
Adafruit_NeoPixel pixels1{NUMPIXELS, 2, NEO_GRB + NEO_KHZ800};
Adafruit_NeoPixel pixels2{NUMPIXELS, 4, NEO_GRB + NEO_KHZ800};
Adafruit_NeoPixel pixels3{NUMPIXELS, 6, NEO_GRB + NEO_KHZ800};
Adafruit_NeoPixel pixels4{NUMPIXELS, 7, NEO_GRB + NEO_KHZ800};
#define BUTTON1_PIN 8
#define BUTTON2_PIN 10
#define BUTTON3_PIN 12
#define BUTTON4_PIN 13


//Global vars
#define FIELD_SIZE 4
int cube[4][4][4] = {0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,
                    0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,
                    0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,
                    0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0};

#define EMPTY_FIELD 0
#define RED 1
#define GREEN 2
#define BLUE 3
#define SELECTED_FIELD 4


void setup() {
  pixels1.begin(); // INITIALIZE NeoPixel strip object (REQUIRED)
  pixels2.begin();
  pixels3.begin();
  pixels4.begin();

  initButtons();

  Serial.begin(9600);
  Serial.print("Boot Completed");
}


void loop() {
  gameChooserLoop();
}

//Display
void show(int fieldStatus[4][4][4]){
  pixels1.clear();
  pixels2.clear();
  pixels3.clear();
  pixels4.clear();
  showText("\nStatus: \n");
  for (int z = 0; z < FIELD_SIZE; z++)
  {
    for (int y = 0; y < FIELD_SIZE; y++)
    {
      for (int x = 0; x < FIELD_SIZE; x++)
      {
        int fieldValue = fieldStatus[x][y][z];
        
        if (fieldValue == EMPTY_FIELD)showText("O");
        if (fieldValue == RED)showText("R");
        if (fieldValue == GREEN)showText("G");
        if (fieldValue == SELECTED_FIELD)showText("S");
        if (fieldValue == BLUE)showText("B");

        showText(" ");
        
        setPixel(z, x, y , fieldValue);
      }
      showText("\n");
    }
    showText("\n");
  }

  pixels1.show();
  pixels2.show();
  pixels3.show();
  pixels4.show();
}
void setPixel(int ebene, int x,int y, int value)
  {
    int number = -1;
    if(ebene % 2 == 0){
      if(y == 0){
        if(x == 3) number = 6;
        else if(x == 2) number = 4;
        else if(x == 1) number = 2;
        else if(x == 0) number = 0;
      }
      if(y == 1){
        if(x == 3) number = 7;
        else if(x == 2) number = 9;
        else if(x == 1) number = 11;
        else if(x == 0) number = 13;
      }
      if(y == 2){
        if(x == 3) number = 20;
        else if(x == 2) number = 18;
        else if(x == 1) number = 16;
        else if(x == 0) number = 14;
      }
      if(y == 3){
        if(x == 3) number = 21;
        else if(x == 2) number = 23;
        else if(x == 1) number = 25;
        else if(x == 0) number = 27;
      }
  
    }else{
      if(y == 0){
        if(x == 3) number = 21;
        else if(x == 2) number = 23;
        else if(x == 1) number = 25;
        else if(x == 0) number = 27;
      }
      if(y == 1){
        if(x == 3) number = 20;
        else if(x == 2) number = 18;
        else if(x == 1) number = 16;
        else if(x == 0) number = 14;
      }
      if(y == 2){
        if(x == 3) number = 7;
        else if(x == 2) number = 9;
        else if(x == 1) number = 11;
        else if(x == 0) number = 13;
      }
      if(y == 3){
        if(x == 3) number = 6;
        else if(x == 2) number = 4;
        else if(x == 1) number = 2;
        else if(x == 0) number = 0;
      }
    }


    //Farbe aussuchen
    int red = 0;
    int green = 0;
    int blue = 0;
    if(value == EMPTY_FIELD){
      red = 0;
      green = 0;
      blue = 0;
    }
    else if(value == RED){
      red = 255;
      green = 0;
      blue = 0;
    }
    else if(value == GREEN){
      red = 0;
      green = 255;
      blue = 0;      
    }
    else if(value == BLUE){
      red = 0;
      green = 0;
      blue = 255;      
    }
    else if(value == SELECTED_FIELD){
      red = 150;
      green = 150;
      blue = 255;
    }else {
      red = 255;
      green = 0;
      blue = 255;
    }

    //Pixel setzen
    if (ebene == 0){
      pixels1.setPixelColor(number, pixels1.Color(red, green, blue));
    }
    if (ebene == 1){
      pixels2.setPixelColor(number, pixels2.Color(red, green, blue));
    }
    if (ebene == 2){
      pixels3.setPixelColor(number, pixels3.Color(red, green, blue));
    }
    if (ebene == 3){
      pixels4.setPixelColor(number, pixels4.Color(red, green, blue));
    }
  }

//Input
bool isButton1Pressed() {
    return isButtonPressed(BUTTON1_PIN);
}

bool isButton2Pressed() {
    return isButtonPressed(BUTTON2_PIN);
}

bool isButton3Pressed() {
    return isButtonPressed(BUTTON3_PIN);
}

bool isButton4Pressed() {
    return isButtonPressed(BUTTON4_PIN);
}
bool isButtonPressed(int pinNumber) {
    if(digitalRead(pinNumber) == 1) return true;
    return false;
}
void initButtons(){
  pinMode(BUTTON1_PIN,INPUT);
  pinMode(BUTTON2_PIN,INPUT);
  pinMode(BUTTON3_PIN,INPUT);
  pinMode(BUTTON4_PIN,INPUT);
}

//Helpers
void showText(const char text[]){
 Serial.print(text);
}

void showNumber(int number){
  Serial.print(number);
}

void copyCube(int fromCube[FIELD_SIZE][FIELD_SIZE][FIELD_SIZE], int toCube[FIELD_SIZE][FIELD_SIZE][FIELD_SIZE]){
  for (int z = 0; z < FIELD_SIZE; z++)
  {
    for (int y = 0; y < FIELD_SIZE; y++)
    {
      for (int x = 0; x < FIELD_SIZE; x++)
      {
        toCube[x][y][z] = fromCube[x][y][z];
      }
    }
  }
}

void showErrorScreen(){
  int oCo = RED;
  int tempCube[FIELD_SIZE][FIELD_SIZE][FIELD_SIZE] = {oCo,oCo,oCo,oCo,oCo,oCo,oCo,oCo,oCo,oCo,oCo,oCo,oCo,oCo,oCo,oCo,
                                                      oCo,oCo,oCo,oCo,oCo,oCo,oCo,oCo,oCo,oCo,oCo,oCo,oCo,oCo,oCo,oCo,
                                                      oCo,oCo,oCo,oCo,oCo,oCo,oCo,oCo,oCo,oCo,oCo,oCo,oCo,oCo,oCo,oCo,
                                                      oCo,oCo,oCo,oCo,oCo,oCo,oCo,oCo,oCo,oCo,oCo,oCo,oCo,oCo,oCo,oCo};
  show(tempCube);
}

void resetCube(){
  int tempCube[FIELD_SIZE][FIELD_SIZE][FIELD_SIZE] = {0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,
                    0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,
                    0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,
                    0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0};
 copyCube(tempCube,cube);
 }
