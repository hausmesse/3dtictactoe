int selectedGame = 0;
boolean gameActive = false;

void gameChooserLoop(){
  showGameIconOrCallMainLoop();
  if(!gameActive){
    if (isButton1Pressed()) {
      while (isButton1Pressed()) {}
      resetCube();
      gameActive = true;
    }else if (isButton2Pressed()) {
      while (isButton2Pressed()) {}
      selectedGame++;
    }
  }
}

void showGameIconOrCallMainLoop(){
  switch (selectedGame) {
    case 0: //DEFAULT MODE
      if(gameActive) tictactoeTwoPlayerLoop();
      else ticTacToeTwoPlayerIcon(cube);
      break;
    case 1: //MINMAX KI MODE
      if(gameActive) tictactoeKiLoop();
      else ticTacToeKiIcon(cube);
      break;
    case 3: //RANDOM KI MODE
      if(gameActive) tictactoeRandomKiLoop();
      else ticTacToeRandomKiIcon(cube);
      break;
    case 4: //SNAKE MODE
      if(gameActive) snakeLoop();
      else snakeIcon(cube);
      break;
    case 5: //PONG MODE
      if(gameActive) pongLoop();
      else pongIcon(cube);
      break;
    default:
      selectedGame = 0;
      break;
  }
  show(cube);
}
