/*---------ICON START*/
void pongIcon(int iconArray[FIELD_SIZE][FIELD_SIZE][FIELD_SIZE]){
  int oCo = SELECTED_FIELD;
  int tempCube[FIELD_SIZE][FIELD_SIZE][FIELD_SIZE] = {0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,
                                                      0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,
                                                      0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,
                                                      0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0};
  tempCube[3][0][0] = oCo;
  tempCube[3][1][0] = oCo;
   tempCube[3][1][2] = BLUE;
  
  
  copyCube(tempCube, iconArray);
}
/*---------ICON END*/

int pongFrame[FIELD_SIZE][FIELD_SIZE][FIELD_SIZE] = {0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,
                                                      0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,
                                                      0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,
                                                      0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0};
int paddlePosition = 1;

int ballPositionX = 3;
int ballPositionY = 3;
int ballPositionZ = 3;

int ballDirection = -1;
int pongStep = 0;

void pongLoop(){

  
  copyCube(cube, pongFrame);
  if (isButton3Pressed()) {
      while (isButton3Pressed()) {/*Warten bis der User den Button wieder loslaesst.*/}
      paddlePosition++;
  }else if (isButton4Pressed()) {
      while (isButton4Pressed()) {/*Warten bis der User den Button wieder loslaesst.*/}
      paddlePosition--;
  }
  
  if(paddlePosition<0)paddlePosition = 0;
  else if(paddlePosition>2)paddlePosition = 2;
  pongFrame[ballPositionX][ballPositionY][ballPositionZ] = BLUE;

  pongFrame[3][paddlePosition][0] = SELECTED_FIELD;
  pongFrame[3][paddlePosition+1][0] = SELECTED_FIELD;
  show(pongFrame);


if(pongStep < 10){
  pongStep++;
  return;
}

 //ball
  if(ballPositionX == 0 && ballPositionZ == 0){
    ballDirection = -1;
    ballPositionZ = 1;
   }else if(ballPositionX == 3 && ballPositionZ == 3){
    if(ballDirection == 1) ballPositionX = 2;
    if(ballDirection == -1) ballPositionZ = 2;
   }else if(ballPositionX == 0 && ballPositionZ == 3){
    if(ballDirection == 1) ballPositionZ = 2;
    if(ballDirection == -1) ballPositionX = 1;
   }else if(ballPositionX == 3){
    ballPositionZ += ballDirection;
   }else  if(ballPositionZ == 3){
    ballPositionX -= ballDirection;
   }else if(ballPositionX == 0){
    ballPositionZ -= ballDirection;
  }

  int r = random(0,4);
  if((abs(r - ballPositionY) <= 1) && ballPositionX == 0) ballPositionY = r;
 //ball

  if(ballPositionX == 3 && ballPositionZ == 0){
    if(ballPositionY == paddlePosition || ballPositionY == paddlePosition +1){
      ballDirection = 1;
    }else{
      showErrorScreen();
      ballDirection = -1;
      ballPositionX = 0;
      ballPositionY = 0;
      ballPositionZ = 0;
      delay(1000);
    }
  }
}
