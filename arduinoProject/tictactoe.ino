int PLAYER1 = RED;
int PLAYER2 = GREEN;
/*---------ICON START*/
void ticTacToeIcon(int iconArray[FIELD_SIZE][FIELD_SIZE][FIELD_SIZE]){
  int oCo = RED;
  int tempCube[FIELD_SIZE][FIELD_SIZE][FIELD_SIZE] = {oCo,0,0,oCo,0,oCo,oCo,0,0,oCo,oCo,0,oCo,0,0,oCo,
                                                      0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,
                                                      0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,
                                                      0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0};
  copyCube(tempCube, iconArray);
}
/*---------ICON END*/

void tictactoeLoop(){

  showText("Player1: ");
  waitForUserToSelectField(cube, PLAYER1);
  show(cube);

  showText("Player2: ");
  waitForUserToSelectField(cube, PLAYER2);
  show(cube);
}

/*
 * Diese Funktion bekommt den aktuellen Stand des Würfels als Parameter.
 * Dann kann der User mit den Buttons frei ein Feld auswählen.
 * Wenn sich der User für ein Feld entschieden hat,
 * wird in das gewählte Feld der Wert von **value** geschrieben und die Funktion wird beendet.
 * 
 * 
 * Alternativer Text:
 * Der User kann auf dem übergebenen Wuerfel mit den 4 Buttons zu jedem Feld navigieren.
 * Durch bestätigen mit Button1 wird der Wert von "value" an die gewählte Position geschrieben.
 * 
*/
void waitForUserToSelectField(int fieldStatus[FIELD_SIZE][FIELD_SIZE][FIELD_SIZE], int value) {
  int tempCube[FIELD_SIZE][FIELD_SIZE][FIELD_SIZE] = {0};
  int stateChanged = true;
  int x = 0;
  int y = 0;
  int z = 0;
  bool isFinished = false;
  while (!isFinished) {
    copyCube(fieldStatus, tempCube);
    tempCube[x][y][z] = SELECTED_FIELD;
    if(stateChanged){
      show(tempCube);
      stateChanged = false;
    }
    
    if (isButton1Pressed()) {
      while (isButton1Pressed()) {/*Warten bis der User den Button wieder loslaesst.*/}
      if(fieldStatus[x][y][z] == EMPTY_FIELD){
        isFinished = true;
      }else{
        showErrorScreen();
        delay(1000);
      }
      stateChanged = true;
    }
    else if (isButton2Pressed()) {
      while (isButton2Pressed()) {}
      x = ( x+ 1)%FIELD_SIZE;
      stateChanged = true;
    }
    else if (isButton3Pressed()) {
      while (isButton3Pressed()) {}
      y = ( y+ 1)%FIELD_SIZE;
      stateChanged = true;
    }
    else if (isButton4Pressed()) {
      while (isButton4Pressed()) {}
      z = ( z+ 1)%FIELD_SIZE;
      stateChanged = true;
    }
  }

  //Ergebnis dem original array zuweisen
  fieldStatus[x][y][z] = value;
}
