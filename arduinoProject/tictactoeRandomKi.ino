/*---------ICON START*/
void ticTacToeRandomKiIcon(int iconArray[FIELD_SIZE][FIELD_SIZE][FIELD_SIZE]){
  int oCo = BLUE;
  int tempCube[FIELD_SIZE][FIELD_SIZE][FIELD_SIZE] = {0,0,0,0,0,0,0,0,oCo,oCo,oCo,oCo,0,0,0,0,
                                                      0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,
                                                      0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,
                                                      oCo,oCo,oCo,oCo,0,oCo,oCo,0,oCo,0,0,oCo,0,0,0,0};
  copyCube(tempCube, iconArray);
}
/*---------ICON END*/

void tictactoeRandomKiLoop(){

  showText("Player1: ");
  waitForUserToSelectField(cube, PLAYER1);
  show(cube);

  showText("KI: ");
  kiDoMove(cube);
  show(cube);
}


void kiDoMove(int fieldStatus[FIELD_SIZE][FIELD_SIZE][FIELD_SIZE]){
  int x = 0;
  int y = 0;
  int z = 0;
  do {
    x = random(0, FIELD_SIZE - 1);
    y = random(0, FIELD_SIZE - 1);
    z = random(0, FIELD_SIZE - 1);
  }while (fieldStatus[x][y][z] != EMPTY_FIELD);
  
  fieldStatus[x][y][z] = PLAYER2;
}
