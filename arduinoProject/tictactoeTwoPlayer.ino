/*---------ICON START*/
void ticTacToeTwoPlayerIcon(int iconArray[FIELD_SIZE][FIELD_SIZE][FIELD_SIZE]){
  int r = RED;
  int g = GREEN;
  int tempCube[FIELD_SIZE][FIELD_SIZE][FIELD_SIZE] = {r,0,0,r,0,r,r,0,0,r,r,0,r,0,0,r,
                                                      0,g,g,0,g,0,0,g,g,0,0,g,0,g,g,0,
                                                      r,0,0,r,0,r,r,0,0,r,r,0,r,0,0,r,
                                                      0,g,g,0,g,0,0,g,g,0,0,g,0,g,g,0};
  copyCube(tempCube, iconArray);
}
/*---------ICON END*/

void tictactoeTwoPlayerLoop(){

  showText("Player1: ");
  waitForUserToSelectField(cube, PLAYER1);
  show(cube);

  showText("Player2: ");
  waitForUserToSelectField(cube, PLAYER2);
  show(cube);
}
